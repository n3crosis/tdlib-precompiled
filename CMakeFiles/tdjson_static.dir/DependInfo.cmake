# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/dev/node-td/td/td/telegram/td_json_client.cpp" "/home/dev/node-td/td/build/CMakeFiles/tdjson_static.dir/td/telegram/td_json_client.cpp.o"
  "/home/dev/node-td/td/td/telegram/td_log.cpp" "/home/dev/node-td/td/build/CMakeFiles/tdjson_static.dir/td/telegram/td_log.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "TDJSON_STATIC_DEFINE"
  "_FILE_OFFSET_BITS=64"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "."
  "../"
  "../td/generate/auto"
  "../tdutils"
  "tdutils"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/dev/node-td/td/build/CMakeFiles/tdjson_private.dir/DependInfo.cmake"
  "/home/dev/node-td/td/build/CMakeFiles/tdclient.dir/DependInfo.cmake"
  "/home/dev/node-td/td/build/CMakeFiles/tdcore.dir/DependInfo.cmake"
  "/home/dev/node-td/td/build/tdnet/CMakeFiles/tdnet.dir/DependInfo.cmake"
  "/home/dev/node-td/td/build/tddb/CMakeFiles/tddb.dir/DependInfo.cmake"
  "/home/dev/node-td/td/build/tdactor/CMakeFiles/tdactor.dir/DependInfo.cmake"
  "/home/dev/node-td/td/build/sqlite/CMakeFiles/tdsqlite.dir/DependInfo.cmake"
  "/home/dev/node-td/td/build/tdutils/CMakeFiles/tdutils.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
