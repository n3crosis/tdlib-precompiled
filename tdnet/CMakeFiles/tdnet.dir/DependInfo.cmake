# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/dev/node-td/td/tdnet/td/net/GetHostByNameActor.cpp" "/home/dev/node-td/td/build/tdnet/CMakeFiles/tdnet.dir/td/net/GetHostByNameActor.cpp.o"
  "/home/dev/node-td/td/tdnet/td/net/HttpChunkedByteFlow.cpp" "/home/dev/node-td/td/build/tdnet/CMakeFiles/tdnet.dir/td/net/HttpChunkedByteFlow.cpp.o"
  "/home/dev/node-td/td/tdnet/td/net/HttpConnectionBase.cpp" "/home/dev/node-td/td/build/tdnet/CMakeFiles/tdnet.dir/td/net/HttpConnectionBase.cpp.o"
  "/home/dev/node-td/td/tdnet/td/net/HttpContentLengthByteFlow.cpp" "/home/dev/node-td/td/build/tdnet/CMakeFiles/tdnet.dir/td/net/HttpContentLengthByteFlow.cpp.o"
  "/home/dev/node-td/td/tdnet/td/net/HttpFile.cpp" "/home/dev/node-td/td/build/tdnet/CMakeFiles/tdnet.dir/td/net/HttpFile.cpp.o"
  "/home/dev/node-td/td/tdnet/td/net/HttpInboundConnection.cpp" "/home/dev/node-td/td/build/tdnet/CMakeFiles/tdnet.dir/td/net/HttpInboundConnection.cpp.o"
  "/home/dev/node-td/td/tdnet/td/net/HttpOutboundConnection.cpp" "/home/dev/node-td/td/build/tdnet/CMakeFiles/tdnet.dir/td/net/HttpOutboundConnection.cpp.o"
  "/home/dev/node-td/td/tdnet/td/net/HttpQuery.cpp" "/home/dev/node-td/td/build/tdnet/CMakeFiles/tdnet.dir/td/net/HttpQuery.cpp.o"
  "/home/dev/node-td/td/tdnet/td/net/HttpReader.cpp" "/home/dev/node-td/td/build/tdnet/CMakeFiles/tdnet.dir/td/net/HttpReader.cpp.o"
  "/home/dev/node-td/td/tdnet/td/net/Socks5.cpp" "/home/dev/node-td/td/build/tdnet/CMakeFiles/tdnet.dir/td/net/Socks5.cpp.o"
  "/home/dev/node-td/td/tdnet/td/net/SslFd.cpp" "/home/dev/node-td/td/build/tdnet/CMakeFiles/tdnet.dir/td/net/SslFd.cpp.o"
  "/home/dev/node-td/td/tdnet/td/net/TcpListener.cpp" "/home/dev/node-td/td/build/tdnet/CMakeFiles/tdnet.dir/td/net/TcpListener.cpp.o"
  "/home/dev/node-td/td/tdnet/td/net/Wget.cpp" "/home/dev/node-td/td/build/tdnet/CMakeFiles/tdnet.dir/td/net/Wget.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "_FILE_OFFSET_BITS=64"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../tdnet"
  "../tdutils"
  "tdutils"
  "../tdactor"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/dev/node-td/td/build/tdutils/CMakeFiles/tdutils.dir/DependInfo.cmake"
  "/home/dev/node-td/td/build/tdactor/CMakeFiles/tdactor.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
